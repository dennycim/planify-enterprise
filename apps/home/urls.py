# -*- encoding: utf-8 -*-


from django.urls import path, re_path
from apps.home import views
from apps.home.views import ClientListView

urlpatterns = [

    # The home page
    path('', views.AllModelsView.as_view(), name='home'),
    # Matches any html file
    # re_path(r'^.*\.*', views.pages, name='pages'),
    # ----------------------   CLIENTS URL   ----------------------------
    path('clients/', views.ClientListView.as_view(), name='client-list'),
    path('clients/upd/<int:pk>/', views.ClientUpdateView.as_view(), name='client-update'),
    path('clients/new/', views.ClientCreateView.as_view(), name='client-create'),
    path('delete_modal_invoice/<int:pk>', views.delete_modal_client, name='client-delete-modal'),

    # ----------------------   INVOICES URL   ----------------------------
    path('invoices/', views.InvoiceListView.as_view(), name='invoice-list'),
    path('invoices/upd/<int:pk>/', views.InvoiceUpdateView.as_view(), name='invoice-update'),
    path('invoices/new/', views.InvoiceCreateView.as_view(), name='invoice-create'),
    path('delete_modal_invoice/<int:pk>', views.delete_modal_invoice, name='invoice-delete-modal'),
    path('invoice_print/<int:pk>/', views.InvoicePrintView.as_view(), name='invoice_print_view'),

    # ----------------------   EMPLOYEES URL   ----------------------------
    path('employees/', views.EmployeeListView.as_view(), name='employee-list'),
    path('employee/edit/<int:pk>/', views.EmployeeEditView.as_view(), name='employee-edit'),
    path('delete_modal_employee/<int:pk>', views.delete_modal_employee, name='employee-delete-modal'),

    # ----------------------   PROJECTS URL   ----------------------------
    path('projects/', views.ProjectListView.as_view(), name='project-list'),
    path('projects/edit/<int:pk>/', views.ProjectEditView.as_view(), name='project-edit'),
    path('projects/new/', views.ProjectCreateView.as_view(), name='project-create'),
    path('delete_modal_project/<int:pk>', views.delete_modal_project, name='project-delete-modal'),

    # ----------------------   TASKS URL   ----------------------------
    path('tasks/', views.TasksListView.as_view(), name='task-list'),
    path('tasks/edit/<int:pk>/', views.TaskEditView.as_view(), name='task-edit'),
    path('tasks/new/', views.TaskCreateView.as_view(), name='task-create'),
    path('delete_modal_task/<int:pk>', views.delete_modal_task, name='task-delete-modal'),
    path('delete_modal_task_cal/<int:pk>', views.delete_modal_task_cal, name='task-delete-modal-cal'),

    # ----------------------   PAYMENTS URL   ----------------------------
    path('payments/', views.PaymentListView.as_view(), name='payment-list'),
    path('payments/edit/<int:pk>/', views.PaymentEditView.as_view(), name='payment-edit'),
    path('payments/new/', views.PaymentCreateView.as_view(), name='payment-create'),
    path('delete_modal_payment/<int:pk>', views.delete_modal_payment, name='payment-delete-modal'),

    # ----------------------   TICKETS URL   ----------------------------
    path('tickets/', views.TicketListView.as_view(), name='ticket-list'),
    path('tickets/edit/<int:pk>/', views.TicketEditView.as_view(), name='ticket-edit'),
    path('tickets/new/', views.TicketCreateView.as_view(), name='ticket-create'),
    path('delete_modal_ticket/<int:pk>', views.delete_modal_ticket, name='ticket-delete-modal'),

    # ----------------------   PRODUCTS URL   ----------------------------
    path('products/', views.ProductListView.as_view(), name='product-list'),
    path('products/edit/<int:pk>/', views.ProductEditView.as_view(), name='product-edit'),
    path('products/new/', views.ProductCreateView.as_view(), name='product-create'),
    path('delete_modal_product/<int:pk>', views.delete_modal_product, name='product-delete-modal'),

    # ----------------------   MARKETING URL   ----------------------------
    path('marketing/', views.MarketingListView.as_view(), name='campaign-list'),
    path('marketing/edit/<int:pk>/', views.MarketingEditView.as_view(), name='campaign-edit'),
    path('marketing/new/', views.MarketingCreateView.as_view(), name='campaign-create'),
    path('delete_modal_marketin/<int:pk>', views.delete_modal_marketing, name='campaign-delete-modal'),

    # ----------------------   REPLENISHMENTS URL   ----------------------------
    path('replenishments/', views.ReplenishmentListView.as_view(), name='replenishment-list'),
    path('replenishments/edit/<int:pk>/', views.ReplenishmentEditView.as_view(), name='replenishment-edit'),
    path('replenishments/new/', views.ReplenishmentCreateView.as_view(), name='replenishment-create'),
    path('delete_modal_replenishment/<int:pk>', views.delete_modal_replenishment, name='replenishment-delete-modal'),

    # ----------------------   SUPPLIERS URL   ----------------------------
    path('suppliers/', views.SupplierListView.as_view(), name='supplier-list'),
    path('suppliers/edit/<int:pk>/', views.SupplierEditView.as_view(), name='supplier-edit'),
    path('suppliers/new/', views.SupplierCreateView.as_view(), name='supplier-create'),
    path('delete_modal_supplier/<int:pk>', views.delete_modal_supplier, name='supplier-delete-modal'),

    # ----------------------   START TEST ZONE   ----------------------------
    path('notifications/', views.show_notifications, name='show_notifications'),
    path('get_notifications/', views.get_notifications, name='get_notifications'),
    path('delete_notification/<int:notification_id>/', views.delete_notification, name='delete_notification'),

    # path('api/events/', views.EventView.as_view(), name='event-list-create'),
    # path('api/events/<int:event_id>/', views.EventView.as_view(), name='event-update-delete'),

    # path('calendar/', views.fullcalendar_view, name='calendar'),
    # path('calendar/', views.EventView.as_view(), name='calendar'),
    # path('all_events/', views.all_events, name='all_events'),
    # path('add_event/', views.add_event, name='add_events'),
    # path('calendar/add/', views.CalendarCreateView.as_view(), name='calendar-create'),
    # path('event/add/', views.EventCreateView.as_view(), name='event-create'),
    # path('update_event/', views.update_event, name='update_event'),

    path('calendar/', views.CalendarView.as_view(), name='calendar'),
    path('all_events/', views.AllEventsView.as_view(), name='all_events'),
    path('add_event/', views.add_event, name='add_event'),
    path('update/', views.update, name='update'),
    path('remove/', views.remove, name='remove'),

    path('chart/', views.line_chart, name='chart-view'),
    # path('plotly/', views.plotly_view, name='plotly_view'),
    path('email/', views.send_email, name='send-mail'),
    # path('create/', views.add_multiple_items, name='create'),
    # # path('edit_profile/', views.edit_profile, name='edit_profile'),
    # path('dash/', views.dashboard, name='dashboard'),
    # ----------------------   END TEST ZONE   ----------------------------
    path('print/<int:invoice_id>/', views.invoice_view_print, name='invoice_view'),
]
