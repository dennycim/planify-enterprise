from apscheduler.schedulers.background import BackgroundScheduler
from .jobs import my_job


def start_scheduler():
    scheduler = BackgroundScheduler()
    scheduler.add_job(my_job, 'interval', seconds=10)
    scheduler.start()
