# -*- encoding: utf-8 -*-
import json
from datetime import datetime, timedelta

import phonenumbers
from django import template
from django.contrib import messages
from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.urls import reverse

# import plotly.express as px
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import generic, View
from django.views.generic import ListView, CreateView, UpdateView, DetailView

from phonenumbers import geocoder, carrier
from .forms import InvoiceForm, ClientForm, EmployeeForm, TaskForm, ProjectForm, PaymentForm, TicketForm, ProductForm, \
    MarketingForm, ReplenishmentForm, SupplierForm
from .models import Invoice, Project, Client, Employee, Task, Ticket, Payment, Product, MarketingCampaign, \
    Replenishment, Supplier, Notification, Event


class ContextDataMixin:
    form_class = None  # define in child classes

    def get_context_data(self, **kwargs):
        # print("Form Class in ContextDataMixin:", self.form_class)  # Debug
        if self.form_class is None:
            raise ValueError("form_class must be set in child class")
        else:
            context = super().get_context_data(**kwargs)
            context['form_get'] = self.form_class()
            object_list = context.get(self.context_object_name, [])
            for obj in object_list:
                obj.update_form = self.form_class(instance=obj)
            context[self.context_object_name] = object_list
            return context


class PostFormMixin:
    form_class = None  # To be overridden by child classes

    def post(self, request):
        # print("Form Class in PostFormMixin:", self.form_class)  # Debugging
        form = self.form_class(request.POST)
        if form.is_valid():
            new_obj = form.save()
            if new_obj.name:
                messages.success(request, f'New record {new_obj.id} - {new_obj.name} added successfully!')
            else:
                messages.success(request, f'New record {new_obj.id} added successfully!')

            return redirect(self.get_success_url())
        else:
            messages.error(request, 'Failed to add new object.')
            return redirect(self.get_failure_url())

    def get_success_url(self):
        return f"{self.model.__name__.lower()}-list"

    def get_failure_url(self):
        return self.get_success_url()


class PrintViewMixin:
    print_template_name = None  # Should be set in the child class

    def get_print_template_name(self):
        if self.print_template_name is None:
            raise ValueError("print_template_name must be set in child class")
        return self.print_template_name

    def get_context_data_for_print(self, **kwargs):
        return {
            'object': self.object,
            # Add more context data here if needed
        }

    def print_view(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data_for_print()
        return render(request, self.get_print_template_name(), context)


@login_required(login_url="/login/")
def index(request):
    context = {'segment': 'index'}

    html_template = loader.get_template('home/index.html')
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]

        if load_template == 'admin':
            return HttpResponseRedirect(reverse('admin:index'))
        context['segment'] = load_template

        html_template = loader.get_template('home/' + load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('home/page-404.html')
        return HttpResponse(html_template.render(context, request))


# ----------- All Models View ------------
class AllModelsView(ContextDataMixin, PostFormMixin, View):
    def get(self, request):
        projects = Project.objects.all()
        # employees = Employee.objects.all()
        clients = Client.objects.all()
        tasks = Task.objects.all()
        # tickets = Ticket.objects.all()
        invoices = Invoice.objects.all()
        notifications = Notification.objects.all().order_by('-created_at')
        # return render(request, 'home/index.html', {'notifications': notifications})

        for project in projects:
            total_days = (project.end - project.start).days
            elapsed_days = (timezone.now().date() - project.start).days
            project.progress = round((elapsed_days / total_days) * 100)

        context = {
            # 'employees': employees,
            'clients': clients,
            'tasks': tasks,
            'projects': projects,
            # 'tickets': tickets,
            'invoices': invoices,
            'notifications': notifications,
        }

        return render(request, 'home/index.html', context)

    def post(self, request):
        if 'employee_form' in request.POST:
            pass

        return redirect('index')  # Redirect back to the same page


# ----------- Client List View ------------
class ClientListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = ClientForm
    model = Client
    # template_name = 'client/client_list.html'
    template_name = 'client/client_list.html'
    context_object_name = 'clients'

    def get_queryset(self):
        queryset = super().get_queryset()

        for client in queryset:
            if '+' in str(client.phone):
                ph = phonenumbers.parse(client.phone)
                # print(client.phone)
                client.carrier = carrier.name_for_number(ph, lang='en')
                client.desc = geocoder.description_for_number(ph, lang='en')
            else:
                client.desc = 'Not valid country code!'
        return queryset


# ----------- Client Update View------------
class ClientUpdateView(LoginRequiredMixin, UpdateView):
    model = Client
    form_class = ClientForm
    template_name = 'client/client_list.html'
    success_url = reverse_lazy('client-list')


# ----------- Client Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_client(request, pk):
    Client.objects.filter(id=pk).delete()
    return redirect('client-list')


# ----------- Client Create View ------------
class ClientCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'client/client_create.html'
    model = Client
    # fields = '__all__'
    form_class = ClientForm
    success_url = reverse_lazy('client-list')
    success_message = 'Client {first} and {last} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(first=self.object.first_name, last=self.object.last_name)


# ----------- Invoice Print Detail View ------------
class InvoicePrintView(PrintViewMixin, DetailView):
    model = Invoice
    template_name = 'invoices/invoice_print.html'  # Detail view template
    print_template_name = 'invoices/invoice_print.html'  # Print view template

    def get(self, request, *args, **kwargs):
        if 'print' in request.GET:
            return self.print_view(request, *args, **kwargs)
        return super().get(request, *args, **kwargs)


# ----------- Invoice List View ------------
class InvoiceListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = InvoiceForm
    model = Invoice
    template_name = 'invoices/invoice_list.html'
    context_object_name = 'invoices'


# ----------- Invoice List Update ------------
class InvoiceUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'invoices/invoice_list.html'
    model = Invoice
    form_class = InvoiceForm
    success_url = reverse_lazy('invoice-list')
    # permission_required = 'invoice.change_invoice'


# ----------- Invoice Create View ------------
class InvoiceCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'invoices/create_invoice.html'
    model = Invoice
    # fields = '__all__'
    form_class = InvoiceForm
    success_url = reverse_lazy('invoice-list')
    success_message = 'You invoice Nr {number} for {customer} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(number=self.object.id, customer=self.object.customer)


# ----------- Invoice Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_invoice(request, pk):
    Invoice.objects.filter(id=pk).delete()
    return redirect('invoice-list')


# ----------- Employee Update View ------------
class EmployeeEditView(LoginRequiredMixin, UpdateView):
    model = Employee
    form_class = EmployeeForm
    # fields = ['hire_date', 'salary', 'position', 'date_of_birth']
    template_name = 'employee/employee_edit.html'
    success_url = reverse_lazy('employee-list')  # Redirect to employee list after successfully updating


# ----------- Employee List View ------------
class EmployeeListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = EmployeeForm
    model = Employee
    template_name = 'employee/employee_list.html'
    context_object_name = 'employees'


# ----------- Employee Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_employee(request, pk):
    Employee.objects.filter(id=pk).delete()
    return redirect('employee-list')


# ----------- Project List View ------------
class ProjectListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = ProjectForm
    model = Project
    template_name = 'project/project_list.html'
    context_object_name = 'projects'


# ----------- Project Update View ------------
class ProjectEditView(LoginRequiredMixin, UpdateView):
    model = Project
    form_class = ProjectForm
    # fields = ['hire_date', 'salary', 'position', 'date_of_birth']
    template_name = 'project/project_edit.html'
    success_url = reverse_lazy('project-list')  # Redirect to employee list after successfully updating


# ----------- Project Create View ------------
class ProjectCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'project/create_project.html'
    model = Project
    # fields = '__all__'
    form_class = ProjectForm
    success_url = reverse_lazy('project-list')
    success_message = 'You Project {name} for {client} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(number=self.object.id, customer=self.object.name)


# ----------- Project Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_project(request, pk):
    Project.objects.filter(id=pk).delete()
    return redirect('project-list')


# ----------- Task List View ------------
class TasksListView(LoginRequiredMixin, ContextDataMixin,  ListView):
    form_class = TaskForm
    model = Task
    template_name = 'tasks/tasks_list.html'
    context_object_name = 'tasks'


# ----------- Task Update View ------------
class TaskEditView(LoginRequiredMixin, UpdateView):
    model = Task
    form_class = TaskForm
    template_name = 'tasks/tasks_list.html'
    success_url = reverse_lazy('task-list')


# ----------- Task Create View ------------
class TaskCreateView(SuccessMessageMixin, CreateView):
    template_name = 'tasks/create_task.html'
    model = Task
    # fields = '__all__'
    form_class = TaskForm
    success_url = reverse_lazy('task-list')
    success_message = 'You Task {id} for {name} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(id=self.object.id, name=self.object.name)


# ----------- Task Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_task(request, pk):
    Task.objects.filter(id=pk).delete()
    return redirect('task-list')


# ----------- Payments List View ------------
class PaymentListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = PaymentForm
    model = Payment
    template_name = 'payments/payments_list.html'
    context_object_name = 'payments'


# ----------- Payment Update View ------------
class PaymentEditView(LoginRequiredMixin, UpdateView):
    model = Payment
    form_class = PaymentForm
    template_name = 'payments/payments_list.html'
    success_url = reverse_lazy('payment-list')


# ----------- Payment Create View ------------
class PaymentCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'payments/create_payment.html'
    model = Payment
    # fields = '__all__'
    form_class = PaymentForm
    success_url = reverse_lazy('payment-list')
    success_message = 'You payment {id} for {amount} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(number=self.object.id, customer=self.object.amount)


# ----------- Payment Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_payment(request, pk):
    Payment.objects.filter(id=pk).delete()
    return redirect('payment-list')


# ----------- Tickets List View ------------
class TicketListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = TicketForm
    model = Ticket
    template_name = 'tickets/tickets_list.html'
    context_object_name = 'tickets'


# ----------- Ticket Update View ------------
class TicketEditView(LoginRequiredMixin, UpdateView):
    model = Ticket
    form_class = TicketForm
    template_name = 'tickets/tickets_list.html'
    success_url = reverse_lazy('ticket-list')


# ----------- Ticket Create View ------------
class TicketCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'tickets/create_ticket.html'
    model = Ticket
    # fields = '__all__'
    form_class = TicketForm
    success_url = reverse_lazy('ticket-list')
    success_message = 'You ticket {id} {name} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(id=self.object.id, name=self.object.name)


# ----------- Ticket Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_ticket(request, pk):
    Ticket.objects.filter(id=pk).delete()
    return redirect('ticket-list')


# ----------- Product List View ------------
class ProductListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = ProductForm
    model = Product
    template_name = 'products/products_list.html'
    context_object_name = 'products'


# ----------- Product Update View ------------
class ProductEditView(LoginRequiredMixin, UpdateView):
    model = Product
    form_class = ProductForm
    template_name = 'products/products_list.html'
    success_url = reverse_lazy('product-list')


# ----------- Product Create View ------------
class ProductCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'products/create_product.html'
    model = Product
    # fields = '__all__'
    form_class = ProductForm
    success_url = reverse_lazy('product-list')
    success_message = 'You product {id} {product_name} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(number=self.object.id, customer=self.object.product_name)


# ----------- Product Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_product(request, pk):
    Product.objects.filter(id=pk).delete()
    return redirect('product-list')


# ----------- Marketing List View ------------
class MarketingListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = MarketingForm
    model = MarketingCampaign
    template_name = 'marketing/campaign_list.html'
    context_object_name = 'campaigns'


# ----------- Marketing Update View ------------
class MarketingEditView(LoginRequiredMixin, UpdateView):
    model = MarketingCampaign
    form_class = MarketingForm
    template_name = 'marketing/campaign_list.html'
    success_url = reverse_lazy('campaign-list')


# ----------- Marketing Create View ------------
class MarketingCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'marketing/create_campaign.html'
    model = MarketingCampaign
    # fields = '__all__'
    form_class = MarketingForm
    success_url = reverse_lazy('campaign-list')
    success_message = 'You product {id} {name} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(id=self.object.id, name=self.object.name)


# ----------- Marketing Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_marketing(request, pk):
    MarketingCampaign.objects.filter(id=pk).delete()
    return redirect('campaign-list')


# ----------- Replenishment List View ------------
class ReplenishmentListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = ReplenishmentForm
    model = Replenishment
    template_name = 'replenishments/replenishments_list.html'
    context_object_name = 'replenishments'


# ----------- Replenishment Update View ------------
class ReplenishmentEditView(LoginRequiredMixin, UpdateView):
    model = Replenishment
    form_class = ReplenishmentForm
    template_name = 'replenishments/replenishments_list.html'
    success_url = reverse_lazy('replenishment-list')


# ----------- Replenishment Create View ------------
class ReplenishmentCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'replenishments/create_replenishment.html'
    model = Replenishment
    # fields = '__all__'
    form_class = ReplenishmentForm
    success_url = reverse_lazy('replenishment-list')
    success_message = 'You replenishment {id} {name} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(id=self.object.id, name=self.object.supplier)


# ----------- Replenishment Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_replenishment(request, pk):
    Replenishment.objects.filter(id=pk).delete()
    return redirect('replenishment-list')


# ----------- Supplier List View ------------
class SupplierListView(LoginRequiredMixin, ContextDataMixin, PostFormMixin, ListView):
    form_class = SupplierForm
    model = Supplier
    template_name = 'suppliers/suppliers_list.html'
    context_object_name = 'suppliers'


# ----------- Supplier Update View ------------
class SupplierEditView(LoginRequiredMixin, UpdateView):
    model = Supplier
    form_class = SupplierForm
    template_name = 'suppliers/suppliers_list.html'
    success_url = reverse_lazy('supplier-list')


# ----------- Supplier Create View ------------
class SupplierCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'suppliers/create_supplier.html'
    model = Supplier
    # fields = '__all__'
    form_class = SupplierForm
    success_url = reverse_lazy('supplier-list')
    success_message = 'You supplier {id} {name} has been created!'

    # permission_required = ''

    def get_success_message(self, cleaned_data):
        return self.success_message.format(id=self.object.id, name=self.object.name)


# ----------- Supplier Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_supplier(request, pk):
    Supplier.objects.filter(id=pk).delete()
    return redirect('supplier-list')


# ----------- Print Modal ------------
@login_required(login_url="/login/")
def invoice_view_print(request, invoice_id):
    # Fetch invoice details from database or generate programmatically
    # This is a simple example. Replace with actual data fetching.
    invoice = {
        'id': invoice_id,
        'date': '2023-09-11',
        'customer': {
            'name': 'John Doe',
            'address': '123 Main St',
            'city': 'Sample City',
            'state': 'ST',
            'zip': '12345'
        },
        'items': [
            {'description': 'Item 1', 'quantity': 2, 'price': 50, 'total': 100},
            {'description': 'Item 2', 'quantity': 1, 'price': 150, 'total': 150},
        ],
        'subtotal': 250,
        'tax': 20,
        'total': 270,
    }

    return render(request, 'invoices/invoice_print.html', {'invoice': invoice})


# ----------- Supplier Create View ------------
class CalendarCreateView(CreateView, ContextDataMixin, PostFormMixin):
    template_name = 'calendar/create_event.html'
    model = Event
    fields = '__all__'
    # form_class = SupplierForm
    success_url = reverse_lazy('home')
    # success_message = 'You supplier {id} {name} has been created!'

    # permission_required = ''

    # def get_success_message(self, cleaned_data):
    #     return self.success_message.format(id=self.object.id, name=self.object.name)


class EventCreateView(CreateView):
    template_name = 'calendar/create_event.html'
    model = Event
    fields = '__all__'
    # form_class = SupplierForm
    success_url = reverse_lazy('home')
    # success_message = 'You supplier {id} {name} has been created!'


def send_email(request):
    subject = 'Hello, Django Email'
    message = 'Here is the message.'
    email_from = 'dennycim@gmail.com'
    recipient_list = ['fstden@gmail.com', ]

    send_mail(subject, message, email_from, recipient_list)
    return HttpResponse("Email sent.")


# ----------- Graphics Tests ------------
def show_notifications(request):
    notifications = Notification.objects.all().order_by('-created_at')
    return render(request, 'notification.html', {'notifications': notifications})


def get_notifications(request):
    notifications = list(Notification.objects.all().order_by('-created_at').values())
    return JsonResponse({'notifications': notifications})


class CalendarView(ListView, ContextDataMixin, PostFormMixin):
    form_class = TaskForm
    model = Task
    template_name = 'home/calendar.html'
    success_url = reverse_lazy('calendar')

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context['form_get'] = self.form_class()
        object_list = context.get(self.context_object_name, [])

        all_events = Task.objects.all()
        today = timezone.now()
        today_tasks = Task.objects.filter(created_at__lte=today, due_date__gte=today)

        for obj in object_list:
            obj.update_form = self.form_class(instance=obj)
        context[self.context_object_name] = object_list

        context['tasks'] = all_events
        context['today_tasks'] = today_tasks

        return context


    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     all_events = Task.objects.all()
    #     today = timezone.now()
    #     today_tasks = Task.objects.filter(created_at__lte=today, due_date__gte=today)
    #
    #     context['tasks'] = all_events
    #     context['today_tasks'] = today_tasks
    #
    #     return context
    #
    # def get(self, request, *args, **kwargs):
    #     context = self.get_context_data()
    #     return render(request, self.template_name, context)


# def calendar(request):
#     all_events = Task.objects.all()
#     today = timezone.now()
#     today_tasks = Task.objects.filter(created_at__lte=today, due_date__gte=today)
#     context = {
#         "events": all_events,
#         "today_tasks": today_tasks,
#     }
#     return render(request, 'home/calendar.html', context)


class AllEventsView(View):

    def get(self, request, *args, **kwargs):
        all_events = Task.objects.all()
        out = []
        for event in all_events:
            out.append({
                'title': event.name,
                'id': event.id,
                'priority': event.priority,
                'start': event.created_at.strftime("%m/%d/%Y, %H:%M:%S"),
                'end': event.due_date.strftime("%m/%d/%Y, %H:%M:%S"),
            })

        return JsonResponse(out, safe=False)


@login_required(login_url="/login/")
def add_event(request):
    start = request.GET.get("start", None)
    end = request.GET.get("end", None)
    title = request.GET.get("title", None)
    event = Task(name=str(title), created_at=start, due_date=end)
    event.save()
    data = {}
    return JsonResponse(data)


@login_required(login_url="/login/")
def update(request):
    start = request.GET.get("start", None)
    end = request.GET.get("end", None)
    title = request.GET.get("title", None)
    id = request.GET.get("id", None)
    event = Task.objects.get(id=id)
    event.created_at = start
    event.due_date = end
    event.name = title
    event.save()
    data = {}
    return JsonResponse(data)


@login_required(login_url="/login/")
def remove(request):
    id = request.GET.get("id", None)
    event = Task.objects.get(id=id)
    event.delete()
    data = {}
    return JsonResponse(data)


# ----------- Task Delete Modal ------------
@login_required(login_url="/login/")
def delete_modal_task_cal(request, pk):
    Task.objects.filter(id=pk).delete()
    return redirect('calendar')


# def calendar_view(request):
#     events = Event.objects.all()
#     return render(request, 'home/calendar.html', {'events': events})

# class EventView(View):
#     def get(self, request, event_id=None):
#         if event_id:
#             # Handle a single event
#             event = get_object_or_404(Event, id=event_id)
#             serialized_event = {
#                 'id': event.id,
#                 'name': event.name,
#                 'start': event.start.isoformat(),
#                 'end': event.end.isoformat(),
#                 'allDay': 'true',
#                 'editable': 'true',
#             }
#             return JsonResponse(serialized_event)
#         else:
#             # Handle all events
#             events = Event.objects.all()
#             serialized_events = [
#                 {
#                     'id': event.id,
#                     'name': event.name,
#                     'start': event.start.isoformat(),
#                     'end': event.end.isoformat(),
#                     'allDay': 'true',
#                     'editable': 'true',
#                 }
#                 for event in events
#             ]
#             return render(request, 'home/calendar.html', {'events': json.dumps(serialized_events)})
#
#     # def get(self, request):
#     #     events = Event.objects.all()
#     #     serialized_events = []
#     #
#     #     for event in events:
#     #         serialized_event = {
#     #             'id': event.id,
#     #             'name': event.name,
#     #             'start': event.start.isoformat(),
#     #             'end': event.end.isoformat()
#     #         }
#     #         serialized_events.append(serialized_event)
#     #
#     #     # return render(request, 'calendar.html', {'events': json.dumps(serialized_events)})
#     #
#     #     return render(request, 'home/calendar.html', {'events': json.dumps(serialized_events)})
#
#     def post(self, request):
#         data = json.loads(request.body)
#         event = Event.objects.create(
#             name=data['name'],
#             start=data['start'],
#             end=data['end']
#         )
#         return JsonResponse({'id': event.id, 'name': event.name, 'start': event.start, 'end': event.end})
#
#     def put(self, request, event_id):
#         event = Event.objects.get(id=event_id)
#         data = json.loads(request.body)
#         event.name = data.get('name', event.name)
#         event.start = data.get('start', event.start)
#         event.end = data.get('end', event.end)
#         event.save()
#         return HttpResponse(status=204)
#
#     def delete(self, request, event_id):
#         Event.objects.get(id=event_id).delete()
#         return HttpResponse(status=204)


# def fullcalendar_view(request):
#     tasks = Event.objects.all()
#     serialized_tasks = serialize('json', tasks)
#
#     # # Let's assume that the visitor uses an iPhone...
#     # request.user_agent.is_mobile # returns True
#     # request.user_agent.is_tablet # returns False
#     # request.user_agent.is_touch_capable # returns True
#     # request.user_agent.is_pc # returns False
#     # request.user_agent.is_bot # returns False
#
#     if request.user_agent.is_mobile:
#         print("mobile")
#     else:
#         print("pc")
#
#     context = {
#         'tasks_json': serialized_tasks
#     }
#
#     return render(request, 'home/calendar.html', context)

#
# def all_events(requests):
#     events = Event.objects.all()
#
#     out = []
#     for event in events:
#         out.append({
#             'id': event.id,
#             'title': event.name,
#             'start': event.start.strftime("%Y-%m-%d %H:%M:%S"),
#             'end': event.end.strftime("%Y-%m-%d %H:%M:%S"),
#         })
#     return JsonResponse({'events': out})
#     # return JsonResponse(out, safe=False)

#
# def adjust_datetime(date_str):
#     date_part, time_part = date_str.split(", ")
#     if time_part == "24:00:00":
#         adjusted_date = datetime.strptime(date_part, "%m/%d/%Y") + timedelta(days=0)
#         return adjusted_date.strftime("%Y-%m-%d 00:00:00")
#     else:
#         datetime_object = datetime.strptime(date_str, "%m/%d/%Y, %H:%M:%S")
#         return datetime_object.strftime("%Y-%m-%d %H:%M:%S")
#
#
# def add_event(request):
#     title = request.GET.get('title')
#     start = request.GET.get('start')
#     end = request.GET.get('end')
#     start_dt = adjust_datetime(start)
#     end_dt = adjust_datetime(end)
#
#     print(f'{title} - {start_dt} - {end_dt}')
#     # try:
#     #     start_dt = adjust_datetime(start)
#     #     end_dt = adjust_datetime(end)
#     #
#     #     print(f'{title} - {start_dt} - {end_dt}')
#     # except ValueError:
#     #     print("Error")
#     #     return JsonResponse({'status': 'Invalid date format'})
#
#     event = Event(name=str(title), start=start_dt, end=end_dt)
#     event.save()
#
#     return JsonResponse({'status': 'Added Successfully'})
#
#
# def update_event(request):
#     if request.method == 'POST':
#         # Handle updating event here, e.g., change the date/time
#         return JsonResponse({"status": "success"})


def delete_notification(request, notification_id):
    try:
        notification = Notification.objects.get(id=notification_id)
        notification.delete()
        return JsonResponse({'status': 'success'})
    except Notification.DoesNotExist:
        return JsonResponse({'status': 'error'}, status=404)


# ----------- Graphics Tests ------------
def line_chart(request):
    # Your data goes here
    labels = [0, 1, 2, 3, 4, 5]
    data = [0, 1, 4, 9, 16, 25]

    context = {
        'labels': labels,
        'data': data,
    }
    return render(request, 'line_chart.html', context)

# ----------- Plotly Tests ------------
# def plotly_view(request):
#     # Create a sample Plotly figure
#     fig = px.scatter(x=[1, 2, 3, 4], y=[10, 11, 9, 12], title="Sample Plotly Visualization")
#
#     # Convert the Plotly figure to HTML
#     plotly_html = fig.to_html(full_html=False)
#
#     return render(request, 'plotly/plotly_view.html', {'plotly_html': plotly_html})

# ----------- TESTS ZONE END ------------
