from enum import Enum

from django.contrib.auth.models import AbstractUser, User
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone

STATUS = (
    ('open', 'Open'),
    ('in_progress', 'In Progress'),
    ('closed', 'Closed'),
)
class Status(Enum):
    OPEN = 'Open'
    IN_PROGRESS = 'In Progress'
    CLOSED = 'Closed'

class Priority(Enum):
    LOW = 'Low'
    MEDIUM = 'Medium'
    HIGH = 'High'


# Customer Management
class Client(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(null=True)
    phone = models.CharField(max_length=20)
    address = models.TextField(null=True)

    def __str__(self):
        return f"{self.first_name} - {self.last_name}"


# Financial Management
class Invoice(models.Model):
    id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Client, on_delete=models.CASCADE)
    date = models.DateField(null=True, blank=True)
    total = models.DecimalField(max_digits=10, decimal_places=2)
    STATUS_CHOICES = [('Unpaid', 'Unpaid'), ('Paid', 'Paid')]
    status = models.CharField(max_length=10, choices=STATUS_CHOICES)

    def __str__(self):
        return f'{self.customer}: ({self.date}) - {self.status}'


# Employee Management
class Employee(AbstractUser):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    hire_date = models.DateField(null=True)
    salary = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    position = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=True, blank=True)
    profile_image = models.ImageField(upload_to='profiles/', null=True, blank=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class MarketingCampaign(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField(null=True)
    start = models.DateField(null=True)
    end = models.DateField(null=True)
    budget = models.DecimalField(null=True, max_digits=10, decimal_places=2)

    def update_budget(self, amount):
        self.budget -= amount
        self.save()

    def __str__(self):
        return f'{self.name}'


class Project(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    budget = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    description = models.TextField(null=True)
    start = models.DateField(null=True)
    end = models.DateField(null=True)
    marketing_campaign = models.ForeignKey(MarketingCampaign, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.client}: {self.name}'


class Ticket(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    status = models.CharField(max_length=50, choices=STATUS)
    customer = models.ForeignKey(Client, on_delete=models.CASCADE)
    description = models.TextField(null=True)
    opening_date = models.DateField(null=True, blank=True)
    closing_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return f'{self.name} - {self.customer} - ({self.status})'


# Stock Management
class Product(models.Model):
    id = models.AutoField(primary_key=True)
    product_name = models.CharField(max_length=200)
    description = models.TextField(null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock_quantity = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.product_name} - {self.stock_quantity} un/m'


class Task(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True, blank=True)
    # status = models.CharField(max_length=50, choices=STATUS)
    status = models.CharField(
        max_length=50,
        choices=[(tag.name, tag.value) for tag in Status],
        default=Status.OPEN.name,
    )
    priority = models.CharField(
        max_length=50,
        choices=[(tag.name, tag.value) for tag in Priority],
        default=Priority.MEDIUM.name,
    )
    product = models.ForeignKey(Product, related_name='tasks', on_delete=models.SET_NULL, null=True, blank=True)
    quantity = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    due_date = models.DateTimeField(null=True)

    def __str__(self):
        return f'{self.name} '

    def save(self, *args, **kwargs):
        if self.quantity != 0:  # Skip stock check if quantity is 0
            # Check for a new task or change in product or quantity for an existing task
            if self.pk is None or (self.pk and (
                    Task.objects.get(pk=self.pk).product != self.product or Task.objects.get(
                pk=self.pk).quantity != self.quantity)):
                # Decrease stock_quantity of the associated product by the selected quantity
                if self.product and self.product.stock_quantity >= self.quantity:
                    self.product.stock_quantity -= self.quantity
                    self.product.save()
                else:
                    raise ValueError('Not enough stock quantity available for this product')
        super(Task, self).save(*args, **kwargs)


class LoggedTime(models.Model):
    id = models.AutoField(primary_key=True)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    numbers_of_minutes = models.IntegerField()
    datetime_logged = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f'{self.employee} - {self.task} - {self.numbers_of_minutes // 60}:{self.numbers_of_minutes % 60} h'


class Supplier(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    address = models.TextField()
    phone = models.CharField(max_length=20)

    def __str__(self):
        return f'{self.name} : {self.address}'


class Replenishment(models.Model):
    id = models.AutoField(primary_key=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, null=True, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    received_quantity = models.PositiveIntegerField(null=True)

    def __str__(self):
        return f'{self.product} - {self.received_quantity} ({self.date})'

    def save(self, *args, **kwargs):
        if self.pk is None:
            super(Replenishment, self).save(*args, **kwargs)
            product_obj = Product.objects.get(pk=self.product.pk)
            product_obj.stock_quantity += self.received_quantity
            product_obj.save()


class Payment(models.Model):
    PAYMENT_CHOICES = [
        ('Invoice', 'Invoice'),
        ('Marketing', 'Marketing Campaign')
    ]
    id = models.AutoField(primary_key=True)
    payment_type = models.CharField(max_length=10, choices=PAYMENT_CHOICES, null=True, blank=True)
    invoice = models.ForeignKey(Invoice, null=True, blank=True, on_delete=models.SET_NULL, related_name='payments')
    marketing_campaign = models.ForeignKey(MarketingCampaign, null=True, blank=True, on_delete=models.SET_NULL,
                                           related_name='payments')
    date = models.DateField(null=True, blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    METHOD_CHOICES = [('Cash', 'Cash'), ('Card', 'Card'), ('Transfer', 'Transfer')]
    method = models.CharField(max_length=10, choices=METHOD_CHOICES)

    def __str__(self):
        if self.invoice:
            return f'Invoice: {self.invoice} - {self.amount}'
        elif self.marketing_campaign:
            return f'Marketing: {self.marketing_campaign} - {self.amount}'
        else:
            return f"Payment of {self.amount}"

    def save(self, *args, **kwargs):
        if self.invoice and self.marketing_campaign:
            raise ValueError("Payment can either be linked to an invoice or a marketing campaign, not both.")
        super(Payment, self).save(*args, **kwargs)


class Notification(models.Model):
    message = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message


class Event(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'tbevents'
#
# class History(models.Model):
#     message = models.TextField(max_length=400)
#     user = models.ForeignKey(Employee, on_delete=models.CASCADE)
#
#     created_at = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return self.message
