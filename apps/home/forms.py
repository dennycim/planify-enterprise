from django import forms
# from django.contrib.auth.models import User
from django.forms import TextInput, NumberInput, EmailInput, Textarea, DateInput, Select
from django.contrib.auth import get_user_model
from apps.home.models import Invoice, Client, Employee, Task, Project, Payment, Replenishment, Supplier, LoggedTime, \
    Ticket, Product, MarketingCampaign
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm, UsernameField
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ['customer', 'total', 'date', 'status']

        widgets = {
            'total': NumberInput(attrs={'class': 'form-control', 'placeholder': 'Please enter value'}),
            'date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),  # datetime-local
            'status': Select(attrs={'class': 'form-select'}),
            'customer': Select(attrs={'class': 'form-select'}),
        }


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__'
        widgets = {
            'first_name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter client first_name.'}),
            'last_name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter client last_name.'}),
            'email': EmailInput(attrs={'class': 'form-control', 'placeholder': 'Please enter client email.'}),
            'phone': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter client phone number.'}),
            'address': Textarea(attrs={'class': 'form-control', 'placeholder': 'Please enter client address.'}),
        }


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = ['last_name', 'first_name', 'salary', 'hire_date', 'position', 'profile_image']
        widgets = {
            'username': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter employee username.'}),
            'first_name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter employee last_name.'}),
            'last_name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter employee last_name.'}),
            'email': EmailInput(attrs={'class': 'form-control', 'placeholder': 'Please enter employee email.'}),
            'hire_date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),  # datetime-local
            'salary': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter salary.'}),
            'position': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter position.'}),
            # 'user_permissions': Select(attrs={'class': 'form-control'})
        }


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['name', 'project', 'status', 'priority', 'product', 'quantity', 'due_date']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter task name.'}),
            'project': Select(attrs={'class': 'form-control', 'placeholder': 'Please select project.'}),
            'status': Select(attrs={'class': 'form-control', 'placeholder': 'Please select status.'}),
            'priority': Select(attrs={'class': 'form-control', 'placeholder': 'Please select priority.'}),
            'product': Select(attrs={'class': 'form-control', 'placeholder': 'Please select product.'}),
            'quantity': NumberInput(attrs={'class': 'form-control', 'type': 'number'}),
            'due_date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),

        }


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name', 'client', 'employee', 'budget', 'description', 'start', 'end', 'marketing_campaign']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter project name'}),
            'client': Select(attrs={'class': 'form-control', 'placeholder': 'Please select client'}),
            'employee': Select(attrs={'class': 'form-control', 'placeholder': 'Please select employee'}),
            'budget': NumberInput(attrs={'class': 'form-control', 'type': 'number'}),
            'description': Textarea(attrs={'class': 'form-control', 'placeholder': 'Please enter description.'}),
            'start': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'end': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'marketing_campaign': Select(attrs={'class': 'form-control', 'placeholder': 'Please select campaign'}),
        }


class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = ['payment_type', 'invoice', 'marketing_campaign', 'date', 'amount', 'method']
        widgets = {
            'payment_type': Select(attrs={'class': 'form-control'}),
            'invoice': Select(attrs={'class': 'form-control'}),
            'marketing_campaign': Select(attrs={'class': 'form-control'}),
            'date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'amount': NumberInput(attrs={'class': 'form-control', 'type': 'number'}),
            'method': Select(attrs={'class': 'form-control'}),
        }


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['product_name', 'price', 'stock_quantity', 'description']
        widgets = {
            'product_name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter product name'}),
            'price': NumberInput(attrs={'class': 'form-control', 'type': 'number'}),
            'stock_quantity': NumberInput(attrs={'class': 'form-control', 'type': 'number'}),
            'description': Textarea(attrs={'class': 'form-control', 'placeholder': 'Please enter description.'}),
        }


class MarketingForm(forms.ModelForm):
    class Meta:
        model = MarketingCampaign
        fields = ['name', 'budget', 'description', 'start', 'end']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter campaign name'}),
            'budget': NumberInput(attrs={'class': 'form-control', 'type': 'number'}),
            'description': Textarea(attrs={'class': 'form-control', 'placeholder': 'Please enter description.'}),
            'start': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'end': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
        }


class ReplenishmentForm(forms.ModelForm):
    class Meta:
        model = Replenishment
        fields = ['supplier', 'product', 'received_quantity']
        widgets = {
            'supplier': Select(attrs={'class': 'form-control'}),
            'product': Select(attrs={'class': 'form-control'}),
            'received_quantity': NumberInput(attrs={'class': 'form-control', 'type': 'numer'}),
        }


class SupplierForm(forms.ModelForm):
    class Meta:
        model = Supplier
        fields = ['name', 'address', 'phone']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter supplier name.'}),
            'address': Textarea(attrs={'class': 'form-control', 'placeholder': 'Please enter address.'}),
            'phone': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter phone number.'}),
        }


class LoggedTimeForm(forms.ModelForm):
    class Meta:
        model = LoggedTime
        fields = ['employee', 'task', 'numbers_of_minutes', 'datetime_logged']
        widgets = {
            'employee': Select(attrs={'class': 'form-control'}),
            'task': Select(attrs={'class': 'form-control'}),
            'numbers_of_minutes': NumberInput(attrs={'class': 'form-control', 'type': 'numer'}),
            'date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
        }


class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ['name', 'status', 'customer', 'description', 'opening_date', 'closing_date']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Please enter ticket name.'}),
            'status': Select(attrs={'class': 'form-control'}),
            'customer': Select(attrs={'class': 'form-control'}),
            'description': Textarea(attrs={'class': 'form-control', 'placeholder': 'Please enter description.'}),
            'opening_date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'closing_date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
        }


class UserForm(UserCreationForm):
    class Meta:
        model = User
        # fields = '__all__'
        fields = ['first_name', 'last_name',
                  'username',
                  'email', 'password1', 'password2', 'groups']

    def clean(self):
        cleaned_data = self.cleaned_data
        get_email = cleaned_data['email']  # cleaned_data.get('email')

        check_emails = User.objects.filter(email=get_email)
        if check_emails:
            msg = 'Exista un student cu aceasta adresa de email'
            self._errors['email'] = self.error_class([msg])

        return cleaned_data

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['first_name'].widget.attrs.update({'class': 'form-control',
                                                       'placeholder': 'Please enter you first name',
                                                       'style': 'width:400px;'})

        self.fields['last_name'].widget.attrs.update({'class': 'form-control',
                                                      'placeholder': 'Please enter you last  name'})

        self.fields['email'].widget.attrs.update({'class': 'form-control',
                                                  'placeholder': 'Please enter you email name'})

        self.fields['username'].widget.attrs.update({'class': 'form-control',
                                                     'placeholder': 'Please enter you username'})

        self.fields['password1'].widget.attrs.update({'class': 'form-control',
                                                      'placeholder': 'Please enter you password'})

        self.fields['password2'].widget.attrs.update({'class': 'form-control',
                                                      'placeholder': 'Confirm you password'})
        self.fields['groups'].widget.attrs.update({'class': 'form-control', 'style': 'width:200px;'})


class AuthenticationNewForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Please enter your username',
            # 'style': 'width:400px;',
        })

        self.fields['password'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Please enter your password',
            # 'style': 'width:400px;justify-content: center;    align-items: center;',
        })
