# jobs.py
from django.utils import timezone

from apps.home.models import Notification


def my_job():
    # print("Job is running!")
    Notification.objects.create(message="Job ran at {}".format(timezone.now()))
