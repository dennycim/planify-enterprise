# Generated by Django 4.2.5 on 2023-09-12 13:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0013_alter_employee_profile_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='profile_image',
            field=models.ImageField(blank=True, null=True, upload_to='apps/static/assets/profile'),
        ),
    ]
