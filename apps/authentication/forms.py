# -*- encoding: utf-8 -*-


from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User

from apps.home.models import Employee

# from django.contrib.auth import get_user_model
#
# User = get_user_model()


class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name',
                  # 'username',
                  'email', 'password1', 'password2']

    def clean(self):
        cleaned_data = self.cleaned_data
        get_email = cleaned_data['email']  # cleaned_data.get('email')

        check_emails = User.objects.filter(email=get_email)
        if check_emails:
            msg = 'Exista un student cu aceasta adresa de email'
            self._errors['email'] = self.error_class([msg])

        return cleaned_data

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['first_name'].widget.attrs.update({'class': 'form-control',
                                                       'placeholder': 'Please enter you first name',
                                                       'style': 'width:400px;'})

        self.fields['last_name'].widget.attrs.update({'class': 'form-control',
                                                      'placeholder': 'Please enter you last  name'})

        self.fields['email'].widget.attrs.update({'class': 'form-control',
                                                  'placeholder': 'Please enter you email name'})

        # self.fields['username'].widget.attrs.update({'class': 'form-control',
        #                                                'placeholder': 'Please enter you username'})

        self.fields['password1'].widget.attrs.update({'class': 'form-control',
                                                      'placeholder': 'Please enter you password'})

        self.fields['password2'].widget.attrs.update({'class': 'form-control',
                                                      'placeholder': 'Confirm you password'})

        # self.fields['first_name'].label = 'numele meu'


class AuthenticationNewForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Please enter your username',

        })

        self.fields['password'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Please enter your password',

        })


class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "class": "form-control"
            }
        ))
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "class": "form-control"
            }
        ))


class SignUpForm(UserCreationForm):
    class Meta:
        model = Employee
        fields = ('first_name','last_name','username', 'email', 'password1', 'password2')

    first_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "First Name",
                "class": "form-control"
            }
        ))

    last_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "Last Name",
                "class": "form-control"
            }
        ))

    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "class": "form-control"
            }
        ))
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "placeholder": "Email",
                "class": "form-control"
            }
        ))
    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "class": "form-control"
            }
        ))
    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password check",
                "class": "form-control"
            }
        ))


