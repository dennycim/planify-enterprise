# -*- encoding: utf-8 -*-
from datetime import datetime

from django.conf.global_settings import DEFAULT_FROM_EMAIL
from django.contrib.auth.models import User
from django.core.mail import send_mail
# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy
from django.views.generic import CreateView

from .forms import LoginForm, SignUpForm
from ..home.forms import UserForm
from ..home.models import Employee


def login_view(request):
    form = LoginForm(request.POST or None)

    msg = None

    if request.method == "POST":

        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")
            else:
                msg = 'Invalid credentials'
        else:
            msg = 'Error validating the form'

    return render(request, "accounts/login.html", {"form": form, "msg": msg})


def register_user(request):
    msg = None
    success = False

    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            # user = authenticate(username=username, password=raw_password)

            msg = 'User created successfully.'
            success = True

            subject = 'New account in Planify Enterprise!'
            message = f'You has been registered in Planify Enterprise. You username is: {username}'
            DEFAULT_FROM_EMAIL = 'dennycim@gmail.com'

            send_mail(subject, message, DEFAULT_FROM_EMAIL, ['fstden@gmail.com'])
            # return redirect("/login/")

        else:
            msg = 'Form is not valid'
    else:
        form = SignUpForm()

    return render(request, "accounts/register.html", {"form": form, "msg": msg, "success": success})


class UserCreateView(CreateView):
    template_name = 'accounts/register.html'
    model = Employee
    form_class = UserForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.first_name = new_user.first_name.title()
            new_user.username = new_user.first_name[0] + new_user.last_name
            # new_user.username = f'{new_user.first_name[0].lower()}{new_user.last_name.lower().replace(" ", "")}' \
            #                     f'_{randint(100000, 999999)}'
            new_user.save()

            subject = f'You have been registered {new_user.username}'
            message = f'You have registered account: You username is: {new_user.username}'
            send_mail(subject, message, DEFAULT_FROM_EMAIL, [new_user.email])

            # # History
            # message = f'first_name: {new_user.first_name}, last_name: {new_user.last_name},' \
            #           f'email: {new_user.email}, username: {new_user.username}'
            # user = new_user.id
            # created_at = datetime.now()
            # # History.objects.create(message=message, user_id=user, created_at=created_at)
        return redirect('login')
