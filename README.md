# Planify Enterprise.

Django powered app for planning and organization of a small company.

## Installation..


``` Python
pip install -r requirement.txt
python manage.py createsuperuser
python manage.py runserver 0.0.0.0:8000
```

Next implementation tasks:


- [ ] Invoices graph analyze.
- [ ] Interactive world map.
- [x] Advanced design UI
- [ ] PDF Export
- [ ] API Support
- [ ] Autologout

[//]: # (<img src="main.jpg" width="400"/>)
### Generate secret.py with email_user & email_pass!

![GitLab last commit](https://img.shields.io/gitlab/last-commit/dennycim/planify-enterprise?style=plastic)
